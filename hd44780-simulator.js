function HD44780Simulator() {
	var MAX_ROWS = 2;
	var MAX_COLS = 40;
	var DCROM = [
		{"name": "&lt;CGRAM[0]&gt;", "code": [0x00, 0x00, 0x00, 0x00, 0x00]}, // 0x00
		{"name": "&lt;CGRAM[1]&gt;", "code": [0x00, 0x00, 0x00, 0x00, 0x00]}, // 0x01
		{"name": "&lt;CGRAM[2]&gt;", "code": [0x00, 0x00, 0x00, 0x00, 0x00]}, // 0x02
		{"name": "&lt;CGRAM[3]&gt;", "code": [0x00, 0x00, 0x00, 0x00, 0x00]}, // 0x03
		{"name": "&lt;CGRAM[4]&gt;", "code": [0x00, 0x00, 0x00, 0x00, 0x00]}, // 0x04
		{"name": "&lt;CGRAM[5]&gt;", "code": [0x00, 0x00, 0x00, 0x00, 0x00]}, // 0x05
		{"name": "&lt;CGRAM[6]&gt;", "code": [0x00, 0x00, 0x00, 0x00, 0x00]}, // 0x06
		{"name": "&lt;CGRAM[7]&gt;", "code": [0x00, 0x00, 0x00, 0x00, 0x00]}, // 0x07
		{"name": "&lt;CGRAM[0]&gt;", "code": [0x00, 0x00, 0x00, 0x00, 0x00]}, // 0x08
		{"name": "&lt;CGRAM[1]&gt;", "code": [0x00, 0x00, 0x00, 0x00, 0x00]}, // 0x09
		{"name": "&lt;CGRAM[2]&gt;", "code": [0x00, 0x00, 0x00, 0x00, 0x00]}, // 0x0A
		{"name": "&lt;CGRAM[3]&gt;", "code": [0x00, 0x00, 0x00, 0x00, 0x00]}, // 0x0B
		{"name": "&lt;CGRAM[4]&gt;", "code": [0x00, 0x00, 0x00, 0x00, 0x00]}, // 0x0C
		{"name": "&lt;CGRAM[5]&gt;", "code": [0x00, 0x00, 0x00, 0x00, 0x00]}, // 0x0D
		{"name": "&lt;CGRAM[6]&gt;", "code": [0x00, 0x00, 0x00, 0x00, 0x00]}, // 0x0E
		{"name": "&lt;CGRAM[7]&gt;", "code": [0x00, 0x00, 0x00, 0x00, 0x00]}, // 0x0F
		{"name": "&lt;empty&gt;", "code": [0x00, 0x00, 0x00, 0x00, 0x00]}, // 0x10
		{"name": "&lt;empty&gt;", "code": [0x00, 0x00, 0x00, 0x00, 0x00]}, // 0x11
		{"name": "&lt;empty&gt;", "code": [0x00, 0x00, 0x00, 0x00, 0x00]}, // 0x12
		{"name": "&lt;empty&gt;", "code": [0x00, 0x00, 0x00, 0x00, 0x00]}, // 0x13
		{"name": "&lt;empty&gt;", "code": [0x00, 0x00, 0x00, 0x00, 0x00]}, // 0x14
		{"name": "&lt;empty&gt;", "code": [0x00, 0x00, 0x00, 0x00, 0x00]}, // 0x15
		{"name": "&lt;empty&gt;", "code": [0x00, 0x00, 0x00, 0x00, 0x00]}, // 0x16
		{"name": "&lt;empty&gt;", "code": [0x00, 0x00, 0x00, 0x00, 0x00]}, // 0x17
		{"name": "&lt;empty&gt;", "code": [0x00, 0x00, 0x00, 0x00, 0x00]}, // 0x18
		{"name": "&lt;empty&gt;", "code": [0x00, 0x00, 0x00, 0x00, 0x00]}, // 0x19
		{"name": "&lt;empty&gt;", "code": [0x00, 0x00, 0x00, 0x00, 0x00]}, // 0x1A
		{"name": "&lt;empty&gt;", "code": [0x00, 0x00, 0x00, 0x00, 0x00]}, // 0x1B
		{"name": "&lt;empty&gt;", "code": [0x00, 0x00, 0x00, 0x00, 0x00]}, // 0x1C
		{"name": "&lt;empty&gt;", "code": [0x00, 0x00, 0x00, 0x00, 0x00]}, // 0x1D
		{"name": "&lt;empty&gt;", "code": [0x00, 0x00, 0x00, 0x00, 0x00]}, // 0x1E
		{"name": "&lt;empty&gt;", "code": [0x00, 0x00, 0x00, 0x00, 0x00]}, // 0x1F
		{"name": "&#x0020;", "code": [0x00, 0x00, 0x00, 0x00, 0x00]}, /* 0x20   */
		{"name": "&#x0021;", "code": [0x00, 0x00, 0x4F, 0x00, 0x00]}, /* 0x21 ! */
		{"name": "&#x0022;", "code": [0x00, 0x07, 0x00, 0x07, 0x00]}, /* 0x22 " */
		{"name": "&#x0023;", "code": [0x14, 0x7F, 0x14, 0x7F, 0x14]}, /* 0x23 # */
		{"name": "&#x0024;", "code": [0x24, 0x2A, 0x7F, 0x2A, 0x12]}, /* 0x24 $ */
		{"name": "&#x0025;", "code": [0x23, 0x13, 0x08, 0x64, 0x62]}, /* 0x25 % */
		{"name": "&#x0026;", "code": [0x36, 0x49, 0x55, 0x22, 0x50]}, /* 0x26 & */
		{"name": "&#x0027;", "code": [0x00, 0x05, 0x03, 0x00, 0x00]}, /* 0x27 ' */
		{"name": "&#x0028;", "code": [0x00, 0x1C, 0x22, 0x41, 0x00]}, /* 0x28 ( */
		{"name": "&#x0029;", "code": [0x00, 0x41, 0x22, 0x1C, 0x00]}, /* 0x29 ) */
		{"name": "&#x002A;", "code": [0x14, 0x08, 0x3E, 0x08, 0x14]}, /* 0x2A * */
		{"name": "&#x002B;", "code": [0x08, 0x08, 0x3E, 0x08, 0x08]}, /* 0x2B + */
		{"name": "&#x002C;", "code": [0x00, 0x50, 0x30, 0x00, 0x00]}, /* 0x2C , */
		{"name": "&#x002D;", "code": [0x08, 0x08, 0x08, 0x08, 0x08]}, /* 0x2D - */
		{"name": "&#x002E;", "code": [0x00, 0x60, 0x60, 0x00, 0x00]}, /* 0x2E . */
		{"name": "&#x002F;", "code": [0x20, 0x10, 0x08, 0x04, 0x02]}, /* 0x2F / */
		{"name": "&#x0030;", "code": [0x3E, 0x51, 0x49, 0x45, 0x3E]}, /* 0x30 0 */
		{"name": "&#x0031;", "code": [0x00, 0x42, 0x7F, 0x40, 0x00]}, /* 0x31 1 */
		{"name": "&#x0032;", "code": [0x42, 0x61, 0x51, 0x49, 0x46]}, /* 0x32 2 */
		{"name": "&#x0033;", "code": [0x21, 0x41, 0x45, 0x4B, 0x31]}, /* 0x33 3 */
		{"name": "&#x0034;", "code": [0x18, 0x14, 0x12, 0x7F, 0x10]}, /* 0x34 4 */
		{"name": "&#x0035;", "code": [0x27, 0x45, 0x45, 0x45, 0x39]}, /* 0x35 5 */
		{"name": "&#x0036;", "code": [0x3C, 0x4A, 0x49, 0x49, 0x30]}, /* 0x36 6 */
		{"name": "&#x0037;", "code": [0x03, 0x01, 0x71, 0x09, 0x07]}, /* 0x37 7 */
		{"name": "&#x0038;", "code": [0x36, 0x49, 0x49, 0x49, 0x36]}, /* 0x38 8 */
		{"name": "&#x0039;", "code": [0x06, 0x49, 0x49, 0x29, 0x1E]}, /* 0x39 9 */
		{"name": "&#x003A;", "code": [0x00, 0x36, 0x36, 0x00, 0x00]}, /* 0x3A : */
		{"name": "&#x003B;", "code": [0x00, 0x56, 0x36, 0x00, 0x00]}, /* 0x3B ; */
		{"name": "&#x003C;", "code": [0x08, 0x14, 0x22, 0x41, 0x00]}, /* 0x3C < */
		{"name": "&#x003D;", "code": [0x14, 0x14, 0x14, 0x14, 0x14]}, /* 0x3D = */
		{"name": "&#x003E;", "code": [0x00, 0x41, 0x22, 0x14, 0x08]}, /* 0x3E > */
		{"name": "&#x003F;", "code": [0x02, 0x01, 0x51, 0x09, 0x06]}, /* 0x3F ? */
		{"name": "&#x0040;", "code": [0x32, 0x49, 0x79, 0x41, 0x3E]}, /* 0x40 @ */
		{"name": "&#x0041;", "code": [0x7E, 0x11, 0x11, 0x11, 0x7E]}, /* 0x41 A */
		{"name": "&#x0042;", "code": [0x7F, 0x49, 0x49, 0x49, 0x36]}, /* 0x42 B */
		{"name": "&#x0043;", "code": [0x3E, 0x41, 0x41, 0x41, 0x22]}, /* 0x43 C */
		{"name": "&#x0044;", "code": [0x7F, 0x41, 0x41, 0x22, 0x1C]}, /* 0x44 D */
		{"name": "&#x0045;", "code": [0x7F, 0x49, 0x49, 0x41, 0x41]}, /* 0x45 E */
		{"name": "&#x0046;", "code": [0x7F, 0x09, 0x09, 0x09, 0x01]}, /* 0x46 F */
		{"name": "&#x0047;", "code": [0x3E, 0x41, 0x49, 0x49, 0x7A]}, /* 0x47 G */
		{"name": "&#x0048;", "code": [0x7F, 0x08, 0x08, 0x08, 0x7F]}, /* 0x48 H */
		{"name": "&#x0049;", "code": [0x00, 0x41, 0x7F, 0x41, 0x00]}, /* 0x49 I */
		{"name": "&#x004A;", "code": [0x20, 0x40, 0x41, 0x3F, 0x01]}, /* 0x4A J */
		{"name": "&#x004B;", "code": [0x7F, 0x08, 0x14, 0x22, 0x41]}, /* 0x4B K */
		{"name": "&#x004C;", "code": [0x7F, 0x40, 0x40, 0x40, 0x40]}, /* 0x4C L */
		{"name": "&#x004D;", "code": [0x7F, 0x02, 0x0C, 0x02, 0x7F]}, /* 0x4D M */
		{"name": "&#x004E;", "code": [0x7F, 0x04, 0x08, 0x10, 0x7F]}, /* 0x4E N */
		{"name": "&#x004F;", "code": [0x3E, 0x41, 0x41, 0x41, 0x3E]}, /* 0x4F O */
		{"name": "&#x0050;", "code": [0x7F, 0x09, 0x09, 0x09, 0x06]}, /* 0x50 P */
		{"name": "&#x0051;", "code": [0x3E, 0x41, 0x51, 0x21, 0x5E]}, /* 0x51 Q */
		{"name": "&#x0052;", "code": [0x7F, 0x09, 0x19, 0x29, 0x46]}, /* 0x52 R */
		{"name": "&#x0053;", "code": [0x46, 0x49, 0x49, 0x49, 0x31]}, /* 0x53 S */
		{"name": "&#x0054;", "code": [0x01, 0x01, 0x7F, 0x01, 0x01]}, /* 0x54 T */
		{"name": "&#x0055;", "code": [0x3F, 0x40, 0x40, 0x40, 0x3F]}, /* 0x55 U */
		{"name": "&#x0056;", "code": [0x1F, 0x20, 0x40, 0x20, 0x1F]}, /* 0x56 V */
		{"name": "&#x0057;", "code": [0x3F, 0x40, 0x38, 0x40, 0x3F]}, /* 0x57 W */
		{"name": "&#x0058;", "code": [0x63, 0x14, 0x08, 0x14, 0x63]}, /* 0x58 X */
		{"name": "&#x0059;", "code": [0x07, 0x08, 0x70, 0x08, 0x07]}, /* 0x59 Y */
		{"name": "&#x005A;", "code": [0x61, 0x51, 0x49, 0x45, 0x43]}, /* 0x5A Z */
		{"name": "&#x005B;", "code": [0x00, 0x7F, 0x41, 0x41, 0x00]}, /* 0x5B [ */
		{"name": "&#x00A5;", "code": [0x15, 0x16, 0x7C, 0x16, 0x15]}, /* 0x5C <japanese dollar sign> */
		{"name": "&#x005D;", "code": [0x00, 0x41, 0x41, 0x7F, 0x00]}, /* 0x5D ] */
		{"name": "&#x005E;", "code": [0x04, 0x02, 0x01, 0x02, 0x04]}, /* 0x5E ^ */
		{"name": "&#x005F;", "code": [0x40, 0x40, 0x40, 0x40, 0x40]}, /* 0x5F _ */
		{"name": "&#x0060;", "code": [0x00, 0x01, 0x02, 0x04, 0x00]}, /* 0x60 ` */
		{"name": "&#x0061;", "code": [0x20, 0x54, 0x54, 0x54, 0x78]}, /* 0x61 a */
		{"name": "&#x0062;", "code": [0x7F, 0x48, 0x44, 0x44, 0x38]}, /* 0x62 b */
		{"name": "&#x0063;", "code": [0x38, 0x44, 0x44, 0x44, 0x20]}, /* 0x63 c */
		{"name": "&#x0064;", "code": [0x38, 0x44, 0x44, 0x48, 0x7F]}, /* 0x64 d */
		{"name": "&#x0065;", "code": [0x38, 0x54, 0x54, 0x54, 0x18]}, /* 0x65 e */
		{"name": "&#x0066;", "code": [0x08, 0x7E, 0x09, 0x01, 0x02]}, /* 0x66 f */
		{"name": "&#x0067;", "code": [0x0C, 0x52, 0x52, 0x52, 0x3E]}, /* 0x67 g */
		{"name": "&#x0068;", "code": [0x7F, 0x08, 0x04, 0x04, 0x78]}, /* 0x68 h */
		{"name": "&#x0069;", "code": [0x00, 0x44, 0x7D, 0x40, 0x00]}, /* 0x69 i */
		{"name": "&#x006A;", "code": [0x20, 0x40, 0x44, 0x3D, 0x00]}, /* 0x6A j */
		{"name": "&#x006B;", "code": [0x7F, 0x10, 0x28, 0x44, 0x00]}, /* 0x6B k */
		{"name": "&#x006C;", "code": [0x00, 0x41, 0x7F, 0x40, 0x00]}, /* 0x6C l */
		{"name": "&#x006D;", "code": [0x7C, 0x04, 0x18, 0x04, 0x78]}, /* 0x6D m */
		{"name": "&#x006E;", "code": [0x7C, 0x08, 0x04, 0x04, 0x78]}, /* 0x6E n */
		{"name": "&#x006F;", "code": [0x38, 0x44, 0x44, 0x44, 0x38]}, /* 0x6F o */
		{"name": "&#x0070;", "code": [0x7C, 0x14, 0x14, 0x14, 0x08]}, /* 0x70 p */
		{"name": "&#x0071;", "code": [0x08, 0x14, 0x14, 0x18, 0x7C]}, /* 0x71 q */
		{"name": "&#x0072;", "code": [0x7C, 0x08, 0x04, 0x04, 0x08]}, /* 0x72 r */
		{"name": "&#x0073;", "code": [0x48, 0x54, 0x54, 0x54, 0x20]}, /* 0x73 s */
		{"name": "&#x0074;", "code": [0x04, 0x3F, 0x44, 0x40, 0x20]}, /* 0x74 t */
		{"name": "&#x0075;", "code": [0x3C, 0x40, 0x40, 0x20, 0x7C]}, /* 0x75 u */
		{"name": "&#x0076;", "code": [0x1C, 0x20, 0x40, 0x20, 0x1C]}, /* 0x76 v */
		{"name": "&#x0077;", "code": [0x3C, 0x40, 0x30, 0x40, 0x3C]}, /* 0x77 w */
		{"name": "&#x0078;", "code": [0x44, 0x28, 0x10, 0x28, 0x44]}, /* 0x78 x */
		{"name": "&#x0079;", "code": [0x0C, 0x50, 0x50, 0x50, 0x3C]}, /* 0x79 y */
		{"name": "&#x007A;", "code": [0x44, 0x64, 0x54, 0x4C, 0x44]}, /* 0x7A z */
		{"name": "&#x007B;", "code": [0x00, 0x08, 0x36, 0x41, 0x00]}, /* 0x7B { */
		{"name": "&#x007C;", "code": [0x00, 0x00, 0x7F, 0x00, 0x00]}, /* 0x7C | */
		{"name": "&#x007D;", "code": [0x00, 0x41, 0x36, 0x08, 0x00]}, /* 0x7D } */
		{"name": "&#x2192;", "code": [0x10, 0x10, 0x54, 0x38, 0x10]}, /* 0x7E <right arrow> */
		{"name": "&#x2190;", "code": [0x10, 0x38, 0x54, 0x10, 0x10]}, /* 0x7F <left arrow> */
		{"name": "&lt;empty&gt;", "code": [0x00, 0x00, 0x00, 0x00, 0x00]}, // 0x80
		{"name": "&lt;empty&gt;", "code": [0x00, 0x00, 0x00, 0x00, 0x00]}, // 0x81
		{"name": "&lt;empty&gt;", "code": [0x00, 0x00, 0x00, 0x00, 0x00]}, // 0x82
		{"name": "&lt;empty&gt;", "code": [0x00, 0x00, 0x00, 0x00, 0x00]}, // 0x83
		{"name": "&lt;empty&gt;", "code": [0x00, 0x00, 0x00, 0x00, 0x00]}, // 0x84
		{"name": "&lt;empty&gt;", "code": [0x00, 0x00, 0x00, 0x00, 0x00]}, // 0x85
		{"name": "&lt;empty&gt;", "code": [0x00, 0x00, 0x00, 0x00, 0x00]}, // 0x86
		{"name": "&lt;empty&gt;", "code": [0x00, 0x00, 0x00, 0x00, 0x00]}, // 0x87
		{"name": "&lt;empty&gt;", "code": [0x00, 0x00, 0x00, 0x00, 0x00]}, // 0x88
		{"name": "&lt;empty&gt;", "code": [0x00, 0x00, 0x00, 0x00, 0x00]}, // 0x89
		{"name": "&lt;empty&gt;", "code": [0x00, 0x00, 0x00, 0x00, 0x00]}, // 0x8A
		{"name": "&lt;empty&gt;", "code": [0x00, 0x00, 0x00, 0x00, 0x00]}, // 0x8B
		{"name": "&lt;empty&gt;", "code": [0x00, 0x00, 0x00, 0x00, 0x00]}, // 0x8C
		{"name": "&lt;empty&gt;", "code": [0x00, 0x00, 0x00, 0x00, 0x00]}, // 0x8D
		{"name": "&lt;empty&gt;", "code": [0x00, 0x00, 0x00, 0x00, 0x00]}, // 0x8E
		{"name": "&lt;empty&gt;", "code": [0x00, 0x00, 0x00, 0x00, 0x00]}, // 0x8F
		{"name": "&lt;empty&gt;", "code": [0x00, 0x00, 0x00, 0x00, 0x00]}, // 0x90
		{"name": "&lt;empty&gt;", "code": [0x00, 0x00, 0x00, 0x00, 0x00]}, // 0x91
		{"name": "&lt;empty&gt;", "code": [0x00, 0x00, 0x00, 0x00, 0x00]}, // 0x92
		{"name": "&lt;empty&gt;", "code": [0x00, 0x00, 0x00, 0x00, 0x00]}, // 0x93
		{"name": "&lt;empty&gt;", "code": [0x00, 0x00, 0x00, 0x00, 0x00]}, // 0x94
		{"name": "&lt;empty&gt;", "code": [0x00, 0x00, 0x00, 0x00, 0x00]}, // 0x95
		{"name": "&lt;empty&gt;", "code": [0x00, 0x00, 0x00, 0x00, 0x00]}, // 0x96
		{"name": "&lt;empty&gt;", "code": [0x00, 0x00, 0x00, 0x00, 0x00]}, // 0x97
		{"name": "&lt;empty&gt;", "code": [0x00, 0x00, 0x00, 0x00, 0x00]}, // 0x98
		{"name": "&lt;empty&gt;", "code": [0x00, 0x00, 0x00, 0x00, 0x00]}, // 0x99
		{"name": "&lt;empty&gt;", "code": [0x00, 0x00, 0x00, 0x00, 0x00]}, // 0x9A
		{"name": "&lt;empty&gt;", "code": [0x00, 0x00, 0x00, 0x00, 0x00]}, // 0x9B
		{"name": "&lt;empty&gt;", "code": [0x00, 0x00, 0x00, 0x00, 0x00]}, // 0x9C
		{"name": "&lt;empty&gt;", "code": [0x00, 0x00, 0x00, 0x00, 0x00]}, // 0x9D
		{"name": "&lt;empty&gt;", "code": [0x00, 0x00, 0x00, 0x00, 0x00]}, // 0x9E
		{"name": "&lt;empty&gt;", "code": [0x00, 0x00, 0x00, 0x00, 0x00]}, // 0x9F
		{"name": "&#x0032;", "code": [0x00, 0x00, 0x00, 0x00, 0x00]}, // 0xA0
		{"name": "&#xFF61;", "code": [0x70, 0x50, 0x70, 0x00, 0x00]}, // 0xA1
		{"name": "&#xFF62;", "code": [0x00, 0x00, 0x0F, 0x01, 0x01]}, // 0xA2
		{"name": "&#xFF63;", "code": [0x40, 0x40, 0x78, 0x00, 0x00]}, // 0xA3
		{"name": "&#xFF64;", "code": [0x10, 0x20, 0x40, 0x00, 0x00]}, // 0xA4
		{"name": "&#xFF65;", "code": [0x00, 0x18, 0x18, 0x00, 0x00]}, // 0xA5
		{"name": "&#xFF66;", "code": [0x0A, 0x0A, 0x4A, 0x2A, 0x1E]}, // 0xA6
		{"name": "&#xFF67;", "code": [0x04, 0x44, 0x34, 0x14, 0x0C]}, // 0xA7
		{"name": "&#xFF68;", "code": [0x20, 0x10, 0x78, 0x04, 0x00]}, // 0xA8
		{"name": "&#xFF69;", "code": [0x18, 0x08, 0x4C, 0x48, 0x38]}, // 0xA9
		{"name": "&#xFF6A;", "code": [0x48, 0x48, 0x78, 0x48, 0x48]}, // 0xAA
		{"name": "&#xFF6B;", "code": [0x48, 0x28, 0x18, 0x7C, 0x08]}, // 0xAB
		{"name": "&#xFF6C;", "code": [0x08, 0x7C, 0x08, 0x28, 0x18]}, // 0xAC
		{"name": "&#xFF6D;", "code": [0x40, 0x48, 0x48, 0x78, 0x40]}, // 0xAD
		{"name": "&#xFF6E;", "code": [0x54, 0x54, 0x54, 0x7C, 0x00]}, // 0xAE
		{"name": "&#xFF6F;", "code": [0x18, 0x00, 0x58, 0x40, 0x38]}, // 0xAF
		{"name": "&#xFF70;", "code": [0x08, 0x08, 0x08, 0x08, 0x08]}, // 0xB0
		{"name": "&#xFF71;", "code": [0x01, 0x41, 0x3D, 0x09, 0x07]}, // 0xB1
		{"name": "&#xFF72;", "code": [0x10, 0x08, 0x7C, 0x02, 0x01]}, // 0xB2
		{"name": "&#xFF73;", "code": [0x0E, 0x02, 0x43, 0x22, 0x1E]}, // 0xB3
		{"name": "&#xFF74;", "code": [0x42, 0x42, 0x7E, 0x42, 0x42]}, // 0xB4
		{"name": "&#xFF75;", "code": [0x22, 0x12, 0x0A, 0x7F, 0x02]}, // 0xB5
		{"name": "&#xFF76;", "code": [0x42, 0x3F, 0x02, 0x42, 0x3E]}, // 0xB6
		{"name": "&#xFF77;", "code": [0x0A, 0x0A, 0x7F, 0x0A, 0x0A]}, // 0xB7
		{"name": "&#xFF78;", "code": [0x08, 0x46, 0x42, 0x22, 0x1E]}, // 0xB8
		{"name": "&#xFF79;", "code": [0x04, 0x03, 0x42, 0x3E, 0x02]}, // 0xB9
		{"name": "&#xFF7A;", "code": [0x42, 0x42, 0x42, 0x42, 0x7E]}, // 0xBA
		{"name": "&#xFF7B;", "code": [0x02, 0x4F, 0x22, 0x1F, 0x02]}, // 0xBB
		{"name": "&#xFF7C;", "code": [0x4A, 0x4A, 0x40, 0x20, 0x1C]}, // 0xBC
		{"name": "&#xFF7D;", "code": [0x42, 0x22, 0x12, 0x2A, 0x46]}, // 0xBD
		{"name": "&#xFF7E;", "code": [0x02, 0x3F, 0x42, 0x4A, 0x46]}, // 0xBE
		{"name": "&#xFF7F;", "code": [0x06, 0x48, 0x40, 0x20, 0x1E]}, // 0xBF
		{"name": "&#xFF80;", "code": [0x08, 0x46, 0x4A, 0x32, 0x1E]}, // 0xC0
		{"name": "&#xFF81;", "code": [0x0A, 0x4A, 0x3E, 0x09, 0x08]}, // 0xC1
		{"name": "&#xFF82;", "code": [0x0E, 0x00, 0x4E, 0x20, 0x1E]}, // 0xC2
		{"name": "&#xFF83;", "code": [0x04, 0x45, 0x3D, 0x05, 0x04]}, // 0xC3
		{"name": "&#xFF84;", "code": [0x00, 0x7F, 0x08, 0x10, 0x00]}, // 0xC4
		{"name": "&#xFF85;", "code": [0x44, 0x24, 0x1F, 0x04, 0x04]}, // 0xC5
		{"name": "&#xFF86;", "code": [0x40, 0x42, 0x42, 0x42, 0x40]}, // 0xC6
		{"name": "&#xFF87;", "code": [0x42, 0x2A, 0x12, 0x2A, 0x06]}, // 0xC7
		{"name": "&#xFF88;", "code": [0x22, 0x12, 0x7B, 0x16, 0x22]}, // 0xC8
		{"name": "&#xFF89;", "code": [0x00, 0x40, 0x20, 0x1F, 0x00]}, // 0xC9
		{"name": "&#xFF8A;", "code": [0x78, 0x00, 0x02, 0x04, 0x78]}, // 0xCA
		{"name": "&#xFF8B;", "code": [0x3F, 0x44, 0x44, 0x44, 0x44]}, // 0xCB
		{"name": "&#xFF8C;", "code": [0x02, 0x42, 0x42, 0x22, 0x1E]}, // 0xCC
		{"name": "&#xFF8D;", "code": [0x04, 0x02, 0x04, 0x08, 0x30]}, // 0xCD
		{"name": "&#xFF8E;", "code": [0x32, 0x02, 0x7F, 0x02, 0x32]}, // 0xCE
		{"name": "&#xFF8F;", "code": [0x02, 0x12, 0x22, 0x52, 0x0E]}, // 0xCF
		{"name": "&#xFF90;", "code": [0x00, 0x2A, 0x2A, 0x2A, 0x40]}, // 0xD0
		{"name": "&#xFF91;", "code": [0x38, 0x24, 0x22, 0x20, 0x70]}, // 0xD1
		{"name": "&#xFF92;", "code": [0x40, 0x28, 0x10, 0x28, 0x06]}, // 0xD2
		{"name": "&#xFF93;", "code": [0x0A, 0x3E, 0x4A, 0x4A, 0x4A]}, // 0xD3
		{"name": "&#xFF94;", "code": [0x04, 0x7F, 0x04, 0x14, 0x0C]}, // 0xD4
		{"name": "&#xFF95;", "code": [0x40, 0x42, 0x42, 0x7E, 0x40]}, // 0xD5
		{"name": "&#xFF96;", "code": [0x4A, 0x4A, 0x4A, 0x4A, 0x7E]}, // 0xD6
		{"name": "&#xFF97;", "code": [0x04, 0x05, 0x45, 0x25, 0x1C]}, // 0xD7
		{"name": "&#xFF98;", "code": [0x0F, 0x40, 0x20, 0x1F, 0x00]}, // 0xD8
		{"name": "&#xFF99;", "code": [0x7C, 0x00, 0x7E, 0x40, 0x30]}, // 0xD9
		{"name": "&#xFF9A;", "code": [0x7E, 0x40, 0x20, 0x10, 0x08]}, // 0xDA
		{"name": "&#xFF9B;", "code": [0x7E, 0x42, 0x42, 0x42, 0x7E]}, // 0xDB
		{"name": "&#xFF9C;", "code": [0x0E, 0x02, 0x42, 0x22, 0x1E]}, // 0xDC
		{"name": "&#xFF9D;", "code": [0x42, 0x42, 0x40, 0x20, 0x18]}, // 0xDD
		{"name": "&#xFF9E;", "code": [0x02, 0x04, 0x01, 0x02, 0x00]}, // 0xDE
		{"name": "&#xFF9F;", "code": [0x07, 0x05, 0x07, 0x00, 0x00]}, // 0xDF
		{"name": "&#x03B1;", "code": [0x38, 0x44, 0x48, 0x30, 0x4C]}, /* 0xE0 <alpha> */
		{"name": "&#x00E4;", "code": [0x20, 0x55, 0x54, 0x55, 0x78]}, /* 0xE1 <a with two dots on top> */
		{"name": "&#x03B2;", "code": [0xF8, 0x54, 0x54, 0x54, 0x28]}, /* 0xE2 <beta> */
		{"name": "&#x03B5;", "code": [0x28, 0x54, 0x54, 0x44, 0x20]}, /* 0xE3 <epsilon> */
		{"name": "&#x03BC;", "code": [0xFC, 0x40, 0x40, 0x20, 0x7C]}, /* 0xE4 <mu> */
		{"name": "&#x03C3;", "code": [0x38, 0x44, 0x4C, 0x54, 0x24]}, /* 0xE5 <sigma> */
		{"name": "&#x03C1;", "code": [0xF0, 0x48, 0x44, 0x44, 0x38]}, /* 0xE6 <rho> */
		{"name": "&#x024B;", "code": [0x38, 0x44, 0x44, 0x44, 0xFC]}, /* 0xE7 <g with hook tail> */
		{"name": "&#x221A;", "code": [0x20, 0x40, 0x3C, 0x04, 0x04]}, /* 0xE8 <square root> */
		{"name": "&#x207B;&#x00B9;", "code": [0x04, 0x04, 0x00, 0x0E, 0x00]}, /* 0xE9 <reverse> */
		{"name": "&#x029D;", "code": [0x00, 0x00, 0x04, 0xFD, 0x00]}, /* 0xEA <j with hook tail> */
		{"name": "&#x1D12A;", "code": [0x0A, 0x04, 0x0A, 0x00, 0x00]}, /* 0xEB <double sharp> */
		{"name": "&#x00A2;", "code": [0x18, 0x24, 0x7E, 0x24, 0x10]}, /* 0xEC <sent sign> */
		{"name": "&#x0167;", "code": [0x14, 0x7F, 0x54, 0x40, 0x40]}, /* 0xED <t with middle line> */
		{"name": "&#x1E45;", "code": [0x7C, 0x09, 0x05, 0x05, 0x78]}, /* 0xEE <n with upper line> */
		{"name": "&#x00F6;", "code": [0x38, 0x45, 0x44, 0x45, 0x38]}, /* 0xEF <o with two dots on top> */
		{"name": "&#0070;", "code": [0xFC, 0x48, 0x44, 0x44, 0x38]}, /* 0xF0 <p with two dots on top> */
		{"name": "&#0071;", "code": [0x38, 0x44, 0x44, 0x48, 0xFC]}, /* 0xF1 <q with two dots on top> */
		{"name": "&#x03B8;", "code": [0x3C, 0x4A, 0x4A, 0x4A, 0x3C]}, /* 0xF2 <theta sign> */
		{"name": "&#x221E;", "code": [0x30, 0x28, 0x10, 0x28, 0x18]}, /* 0xF3 <infinity sign> */
		{"name": "&#x03A9;", "code": [0x58, 0x64, 0x04, 0x64, 0x58]}, /* 0xF4 <omega sign> */
		{"name": "&#x00FC;", "code": [0x3C, 0x41, 0x40, 0x21, 0x7C]}, /* 0xF5 <u with two dots on top> */
		{"name": "&#x03A3;", "code": [0x63, 0x55, 0x49, 0x41, 0x41]}, /* 0xF6 <summation sign> */
		{"name": "&#x03C0;", "code": [0x44, 0x3C, 0x04, 0x7C, 0x44]}, /* 0xF7 <pi sign> */
		{"name": "&#x1E8B;", "code": [0x45, 0x29, 0x11, 0x29, 0x45]}, /* 0xF8 <x with upper line> */
		{"name": "&#x0079;", "code": [0x3C, 0x40, 0x40, 0x40, 0xFC]}, /* 0xF9 <y with hook tail> */
		{"name": "&#x5343;", "code": [0x14, 0x14, 0x7C, 0x14, 0x12]}, /* 0xFB <japanese 1 thousand name> */
		{"name": "&#x4E07;", "code": [0x44, 0x3C, 0x14, 0x14, 0x74]}, /* 0xFB <japanese 10 thousand name> */
		{"name": "&#x5186;", "code": [0x7C, 0x14, 0x1C, 0x14, 0x7C]}, /* 0xFC <japanese dollar name> */
		{"name": "&#x00F7;", "code": [0x10, 0x10, 0x54, 0x10, 0x10]}, /* 0xFD <division sign> */
		{"name": "&#x25A1;", "code": [0x00, 0x00, 0x00, 0x00, 0x00]}, /* 0xFE <white block> */
		{"name": "&#x25A0;", "code": [0xFF, 0xFF, 0xFF, 0xFF, 0xFF]} /* 0xFF <black block> */
	];
	var pwPin = document.getElementById("pw");
	var rsPin = document.getElementById("rs");
	var rwPin = document.getElementById("rw");
	var enPin = document.getElementById("en");
	var dPins = [
		document.getElementById("d0"),
		document.getElementById("d1"),
		document.getElementById("d2"),
		document.getElementById("d3"),
		document.getElementById("d4"),
		document.getElementById("d5"),
		document.getElementById("d6"),
		document.getElementById("d7")
	];
	var row = 0;
	var col = 0;
	var offset = 0;
	var tempDone = true;
	var tempByte = 0;
	var CGRAMIndex = -1;
	var CGRAMRow = 0;
	var DDRAM = [
		[0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20],
		[0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20]
	];
	var bits = true;
	var rows = false;
	var dots = false;
	var screenOn = false;
	var cursorOn = false;
	var blinkOn = false;
	var moveRight = true;
	var screenMove = false;
	var padChar = function (string, length, char) {
		string = "" + string;
		while (string.length < length) {
			string = char + string;
		}
		return string;
	};
	var replaceAll = function (string, search, replace) {
		while (string.indexOf(search) > -1) {
			string = string.replace(search, replace);
		}
		return string;
	};
	var getByte = function () {
		var data = 0;
		for (var i = 0; i < dPins.length; i++) {
			if (isPinHigh(dPins[i])) {
				data |= (1 << i);
			}
		}
		return data;
	};
	var updatePowerInfo = function (pin) {
		document.getElementById("pw-info").innerHTML
			= "Power : "
			+ (isPinHigh(pin) ? "ON" : "OFF");
	};
	var updatePeriodInfo = function (b, done) {
		document.getElementById("period-info").innerHTML
			= "Period : "
			+ (b ? "7 ~ 0" : (done ? "7 ~ 4" : "3 ~ 0"));
	};
	var updateBitsInfo = function (v) {
		document.getElementById("bits-info").innerHTML
			= "Bits : "
			+ (v ? "8" : "4");
	};
	var updateRowsInfo = function (v) {
		document.getElementById("rows-info").innerHTML
			= "Rows : "
			+ (v ? "2" : "1");
	};
	var updateDotsInfo = function (v) {
		document.getElementById("dots-info").innerHTML
			= "Dots : "
			+ (v ? "10" : "8")
			+ "x5"
			+ (v ? " (N/A)" : "");
	};
	var updateScreenInfo = function (on) {
		document.getElementById("screen-info").innerHTML
			= "Screen : "
			+ (on ? "ON" : "OFF");
	};
	var updateCursorInfo = function (on) {
		document.getElementById("cursor-info").innerHTML
			= "Cursor : "
			+ (on ? "ON" : "OFF");
	};
	var updateBlinkInfo = function (on) {
		document.getElementById("blink-info").innerHTML
			= "Blink : "
			+ (on ? "ON" : "OFF");
	};
	var updateEntryInfo = function (type, direction) {
		document.getElementById("entry-info").innerHTML
			= "Entry : "
			+ (type ? "Screen" : "Cursor")
			+ " moves "
			+ (direction ? "Right" : "Left");
	};
	var updateRegisterInfo = function (pin) {
		document.getElementById("register-info").innerHTML
			= "Register : "
			+ (isPinHigh(pin) ? "Data" : "Instruction");
	};
	var updateModeInfo = function (pin) {
		document.getElementById("mode-info").innerHTML
			= "Mode : "
			+ (isPinHigh(pin) ? "Read" : "Write");
	};
	var updatePositionInfo = function (r, c) {
		document.getElementById("position-info").innerHTML
			= "Row : "
			+ r
			+ " ; Column : "
			+ ((c < 10) ? "&#160;" : "")
			+ c;
	};
	var updateOffsetInfo = function (o) {
		document.getElementById("offset-info").innerHTML
			= "Offset : "
			+ (o < 10 ? "&#160;" : "")
			+ o;
	};
	var updateCGRAMInfo = function (index, row) {
		document.getElementById("cgram-info").innerHTML
			= "CGRAM : "
			+ ((index > -1) ? ("CGRAM[" + index + "] on Row " + row) : "Not Ready");
	};
	var updateDataInfo = function (data) {
		document.getElementById("data-info").innerHTML = "Data : "
			+ replaceAll(padChar(data, 3, " "), " ", "&#160;")
			+ "(dec) ; "
			+ "0x" + padChar(data.toString(16), 2, "0").toUpperCase()
			+ "(hex) ; B"
			+ padChar(data.toString(2), 8, "0")
			+ "(bin)";
	};
	var updateDescriptionInfo = function (description) {
		document.getElementById("description-info").innerHTML
			= "Description : "
			+ description;
	};
	var updateDescriptionReadDDRAMInfo = function (bits) {
		updateDescriptionInfo(
			"Read "
			+ bits
			+ "DDRAM data from cursor position"
			);
	};
	var updateDescriptionReadPositionInfo = function (bits) {
		updateDescriptionInfo(
			"Read "
			+ bits
			+ "position from cursor"
			);
	};
	var updateDescriptionWriteCGRAMInfo = function (index, row) {
		updateDescriptionInfo(
			"Setting custom character to CGRAM["
			+ index
			+ "] on Row "
			+ row
			);
	};
	var updateDescriptionWriteDDRAMInfo = function (data) {
		updateDescriptionInfo(
			"Write '"
			+ DCROM[data].name
			+ "' character to DDRAM"
			);
	};
	var updateDescriptionWritePositionInfo = function (data) {
		var tempData = data & 0x3F;
		if (tempData >= MAX_COLS) {
			tempData = MAX_COLS - 1;
		}
		updateDescriptionInfo(
			"Move to Row "
			+ ((data & 0x40) > 0 ? "1" : "0")
			+ " and Column "
			+ tempData
			);
	};
	var updateDescriptionWriteDCROMInfo = function (data) {
		updateDescriptionInfo(
			"Create custom character to CGRAM at "
			+ ((data >> 3) & 0x07)
			);
	};
	var updateDescriptionWriteFunctionInfo = function (data) {
		updateDescriptionInfo(
			"Function Set : "
			+ ((data & 0x10) > 0 ? "8" : "4")
			+ " Bits ; "
			+ ((data & 0x08) > 0 ? "2" : "1")
			+ " Rows ; "
			+ ((data & 0x04) > 0 ? "10" : "&#160;8")
			+ "x5 Dots"
			);
	};
	var updateDescriptionWriteMovementInfo = function (data) {
		updateDescriptionInfo(
			((data & 0x08) > 0 ? "Screen" : "Cursor")
			+ " moves "
			+ ((data & 0x04) > 0 ? "Right" : "&#160;Left")
			);
	};
	var updateDescriptionWriteScreenInfo = function (data) {
		updateDescriptionInfo(
			"Display Mode : "
			+ "Screen "
			+ ((data & 0x04) > 0 ? "&#160;ON" : "OFF")
			+ " ; Cursor "
			+ ((data & 0x02) > 0 ? "&#160;ON" : "OFF")
			+ " ; Blink "
			+ ((data & 0x01) > 0 ? "&#160;ON" : "OFF")
			);
	};
	var updateDescriptionWriteEntryInfo = function (data) {
		updateDescriptionInfo(
			((data & 0x01) > 0 ? "Screen" : "Cursor")
			+ " moves "
			+ ((data & 0x02) > 0 ? "Right" : "&#160;Left")
			+ " after DDRAM is updated");
	};
	var updateDescriptionWriteResetInfo = function () {
		updateDescriptionInfo("Reset screen");
	};
	var updateDescriptionWriteClearInfo = function () {
		updateDescriptionInfo("Clear DDRAM and Reset screen");
	};
	var updateInfo = function () {
		updatePowerInfo(pwPin);
		updatePeriodInfo(bits, tempDone);
		updateBitsInfo(bits);
		updateRowsInfo(rows);
		updateDotsInfo(dots);
		updateScreenInfo(screenOn);
		updateCursorInfo(cursorOn);
		updateBlinkInfo(blinkOn);
		updateEntryInfo(screenMove, moveRight);
		updateRegisterInfo(rsPin);
		updateModeInfo(rwPin);
		updatePositionInfo(row, col);
		updateOffsetInfo(offset);
		updateCGRAMInfo(CGRAMIndex, CGRAMRow);
		updateDescriptionInfo("None");
		var data = getByte();
		if (bits) {
			updateDataInfo(data);
			if (isPinHigh(rsPin)) {
				if (isPinHigh(rwPin)) {
					updateDescriptionReadDDRAMInfo("");
				} else {
					if (CGRAMIndex > -1) {
						updateDescriptionWriteCGRAMInfo(CGRAMIndex, CGRAMRow);
					} else {
						updateDescriptionWriteDDRAMInfo(data);
					}
				}
			} else {
				if (isPinHigh(rwPin)) {
					updateDescriptionReadPositionInfo("");
				} else {
					if (isPinHigh(dPins[7])) {
						updateDescriptionWritePositionInfo(data);
					} else if (isPinHigh(dPins[6])) {
						updateDescriptionWriteDCROMInfo(data);
					} else if (isPinHigh(dPins[5])) {
						updateDescriptionWriteFunctionInfo(data);
					} else if (isPinHigh(dPins[4])) {
						updateDescriptionWriteMovementInfo(data);
					} else if (isPinHigh(dPins[3])) {
						updateDescriptionWriteScreenInfo(data);
					} else if (isPinHigh(dPins[2])) {
						updateDescriptionWriteEntryInfo(data);
					} else if (isPinHigh(dPins[1])) {
						updateDescriptionWriteResetInfo();
					} else if (isPinHigh(dPins[0])) {
						updateDescriptionWriteClearInfo();
					}
				}
			}
		} else {
			data &= 0x0F;
			updateDataInfo(data);
			if (tempDone) {
				if (isPinHigh(rsPin)) {
					if (isPinHigh(rwPin)) {
						updateDescriptionReadDDRAMInfo("bit-7 to bit-4 ");
					} else {
						if (CGRAMIndex > -1) {
							updateDescriptionWriteCGRAMInfo(CGRAMIndex, CGRAMRow);
						} else {
							updateDescriptionWriteDDRAMInfo(data << 4);
						}
					}
				} else {
					if (isPinHigh(rwPin)) {
						updateDescriptionReadPositionInfo("bit-7 to bit-4 ");
					} else {
						if (isPinHigh(dPins[3])) {
							updateDescriptionWritePositionInfo(data << 4);
						} else if (isPinHigh(dPins[2])) {
							updateDescriptionWriteDCROMInfo(data << 4);
						} else if (isPinHigh(dPins[1])) {
							updateDescriptionWriteFunctionInfo(data << 4);
						} else if (isPinHigh(dPins[0])) {
							updateDescriptionWriteMovementInfo(data << 4);
						}
					}
				}
			} else {
				data |= tempByte;
				if (isPinHigh(rsPin)) {
					if (isPinHigh(rwPin)) {
						updateDescriptionReadDDRAMInfo("bit-3 to bit-0 ");
					} else {
						if (CGRAMIndex > -1) {
							updateDescriptionWriteCGRAMInfo(CGRAMIndex, CGRAMRow);
						} else {
							updateDescriptionWriteDDRAMInfo(data);
						}
					}
				} else {
					if (isPinHigh(rwPin)) {
						updateDescriptionReadPositionInfo("bit-3 to bit-0 ");
					} else {
						if ((data & 0x80) > 0) {
							updateDescriptionWritePositionInfo(data);
						} else if ((data & 0x40) > 0) {
							updateDescriptionWriteDCROMInfo(data);
						} else if ((data & 0x20) > 0) {
							updateDescriptionWriteFunctionInfo(data);
						} else if ((data & 0x10) > 0) {
							updateDescriptionWriteMovementInfo(data);
						} else if ((data & 0x08) > 0) {
							updateDescriptionWriteScreenInfo(data);
						} else if ((data & 0x04) > 0) {
							updateDescriptionWriteEntryInfo(data);
						} else if ((data & 0x02) > 0) {
							updateDescriptionWriteResetInfo();
						} else if ((data & 0x01) > 0) {
							updateDescriptionWriteClearInfo();
						}
					}
				}
			}
		}
	};
	var updateDDRAM = function () {
		var tableRows = [];
		var tableRows0 = document.getElementsByClassName("table-row-0");
		for (var i = 0; i < tableRows0.length; i++) {
			tableRows.push(tableRows0.item(i));
		}
		var tableRows1 = document.getElementsByClassName("table-row-1");
		for (var i = 0; i < tableRows1.length; i++) {
			tableRows.push(tableRows1.item(i));
		}
		for (var r = 0; r < MAX_ROWS; r++) {
			tableRows[r].getElementsByClassName("back").item(0).setAttribute("style", (screenOn && (r == 0 || (r == 1 && rows))) ? "" : "display: none;");
			var tableCols = tableRows[r].getElementsByClassName("table-col");
			for (var c = 0; c < MAX_COLS; c++) {
				var offsetC = (c + offset) % MAX_COLS;
				var byteCols = tableCols.item(offsetC).getElementsByClassName("byte-col");
				tableCols.item(offsetC).getElementsByClassName("cursor").item(0).setAttribute("style", (r == row && c == col && screenOn && cursorOn && (r == 0 || (r == 1 && rows))) ? "" : "display: none;");
				tableCols.item(offsetC).getElementsByClassName("blink").item(0).setAttribute("style", "display: none;");
				for (var x = 0; x < 5; x++) {
					var byteRows = byteCols.item(x).getElementsByClassName("byte-row");
					for (var y = 0; y < 8; y++) {
						byteRows.item(7 - y).setAttribute("style", (((DCROM[DDRAM[r][c]].code[x] & (1 << y)) > 0) && screenOn && (r == 0 || (r == 1 && rows))) ? "" : "display: none;");
					}
				}
			}
		}
		updateInfo();
	};
	var readData = function (data) {
		if (bits) {
			for (var i = 0; i < 8; i++) {
				setPin(dPins[i], (data & (1 << i)) > 0);
			}
			setRowCol(row, col + (moveRight ? 1 : -1));
		} else {
			for (var i = 4; i < 8; i++) {
				setPin(dPins[i], false);
			}
			for (var i = 0; i < 4; i++) {
				setPin(dPins[i], (data & (1 << i)) > 0);
			}
			if (!tempDone) {
				setRowCol(row, col + (moveRight ? 1 : -1));
			}
		}
	};
	var writeData = function (data) {
		if (CGRAMIndex > -1) {
			for (var i = 0; i < 5; i++) {
				DCROM[CGRAMIndex].code[i]
					= DCROM[CGRAMIndex + 8].code[i]
					= (DCROM[CGRAMIndex].code[i] & (0xFF ^ (1 << CGRAMRow))) | ((data & (1 << i)) > 0 ? 1 << CGRAMRow : 0);
			}
			CGRAMRow++;
			CGRAMRow %= 8;
			if (CGRAMRow == 0) {
				CGRAMIndex++;
				CGRAMIndex %= 8;
			}
		} else {
			DDRAM[row][col] = data;
			if (screenMove) {
				setOffset(offset - (moveRight ? 1 : -1));
			}
			setRowCol(row, col + (moveRight ? 1 : -1));
		}
	};
	var readInstruction = function (data) {
		if (bits) {
			setPin(dPins[7], false);
			for (var i = 0; i < 7; i++) {
				setPin(dPins[i], (data & (1 << i)) > 0);
			}
		} else {
			for (var i = 4; i < 8; i++) {
				setPin(dPins[i], false);
			}
			for (var i = 0; i < 4; i++) {
				setPin(dPins[i], (data & (1 << i)) > 0);
			}
		}
	};
	var writeInstruction = function (data) {
		CGRAMIndex = -1;
		CGRAMRow = 0;
		if ((data & 0x80) > 0) {
			setPosition(data);
		} else if ((data & 0x40) > 0) {
			setCGRAM(data);
		} else if ((data & 0x20) > 0) {
			setFunctionMode(data);
		} else if ((data & 0x10) > 0) {
			setCursorScreen(data);
		} else if ((data & 0x08) > 0) {
			setScreenMode(data);
		} else if ((data & 0x04) > 0) {
			setEntryMode(data);
		} else if ((data & 0x02) > 0) {
			reset();
		} else if ((data & 0x01) > 0) {
			clear();
		}
	};
	var setPosition = function (data) {
		setRowCol(((data & 0x40) > 0) ? 1 : 0, (data & 0x3F));
	};
	var setCGRAM = function (data) {
		CGRAMIndex = (data >> 3) & 0x07;
	};
	var setFunctionMode = function (data) {
		bits = ((data & 0x10) > 0);
		rows = ((data & 0x08) > 0);
		dots = ((data & 0x04) > 0);
	};
	var setCursorScreen = function (data) {
		if ((data & 0x08) > 0) {
			setOffset(offset + (((data & 0x04) > 0) ? 1 : -1));
		} else {
			setRowCol(row, col + (((data & 0x04) > 0) ? 1 : -1));
		}
	};
	var setScreenMode = function (data) {
		screenOn = ((data & 0x04) > 0);
		cursorOn = ((data & 0x02) > 0);
		blinkOn = ((data & 0x01) > 0);
	};
	var setEntryMode = function (data) {
		moveRight = ((data & 0x02) > 0);
		screenMove = ((data & 0x01) > 0);
	};
	var reset = function () {
		row = 0;
		col = 0;
		offset = 0;
		CGRAMIndex = -1;
		CGRAMRow = 0;
		tempDone = true;
		tempByte = 0;
	};
	var clear = function () {
		reset();
		for (var i = 0x00; i < 0x08; i++) {
			DCROM[i].code
				= DCROM[i + 8].code
				= [0x00, 0x00, 0x00, 0x00, 0x00];
		}
		for (var r = 0; r < MAX_ROWS; r++) {
			for (var c = 0; c < MAX_COLS; c++) {
				DDRAM[r][c] = 0x20;
			}
		}
	};
	var pwOff = function () {
		clear();
		bits = true;
		rows = false;
		dots = false;
		screenOn = false;
		cursorOn = false;
		blinkOn = false;
		moveRight = true;
		screenMove = false;
	};
	var setOffset = function (o) {
		if (o < 0) {
			o = MAX_COLS - 1;
		} else if (o >= MAX_COLS) {
			o = 0;
		}
		offset = o;
	};
	var setRowCol = function (r, c) {
		if (c < 0) {
			c = MAX_COLS - 1;
			r--;
		} else if (c >= MAX_COLS) {
			c = 0;
			r++;
		}
		col = c;
		row = ((r % MAX_ROWS) + MAX_ROWS) % MAX_ROWS;
	};
	var isPinHigh = function (pin) {
		return (pin.getAttribute("opacity") == "1");
	};
	var setPin = function (pin, status) {
		pin.setAttribute("opacity", (status ? "1" : "0"));
		document.getElementById(pin.id + "-back").setAttribute("xlink:href", "#pin-" + (status ? "high" : "low"));
		document.getElementById(pin.id + "-text").setAttribute("fill", "#" + (status ? "008800" : "880000"));
		updateInfo();
	};
	this.pw = function () {
		setPin(pwPin, !isPinHigh(pwPin));
		document.getElementById("screen").setAttribute("fill-opacity", (isPinHigh(pwPin) ? "0" : "1"));
		if (isPinHigh(pwPin)) {
			pwOff();
			updateDDRAM();
		}
	};
	this.rs = function () {
		setPin(rsPin, !isPinHigh(rsPin));
	};
	this.rw = function () {
		setPin(rwPin, !isPinHigh(rwPin));
	};
	this.ds = function (d) {
		setPin(dPins[d], !isPinHigh(dPins[d]));
	};
	this.en = function () {
		setPin(enPin, !isPinHigh(enPin));
		if (isPinHigh(pwPin) && !isPinHigh(enPin)) {
			if (bits) {
				if (isPinHigh(rwPin)) {
					if (isPinHigh(rsPin)) {
						var data = DDRAM[row][col];
						readData(data);
					} else {
						var data = ((row > 0) ? 0x40 : 0) | col;
						readInstruction(data);
					}
				} else {
					var data = getByte();
					if (isPinHigh(rsPin)) {
						writeData(data);
					} else {
						writeInstruction(data);
					}
				}
			} else {
				if (tempDone) {
					if (isPinHigh(rwPin)) {
						if (isPinHigh(rsPin)) {
							var data = DDRAM[row][col];
							readData((data >> 4) & 0x0F);
						} else {
							var data = ((row > 0) ? 0x40 : 0) | col;
							readInstruction((data >> 4) & 0x0F);
						}
					} else {
						tempByte
							= (isPinHigh(dPins[3]) ? 0x80 : 0)
							| (isPinHigh(dPins[2]) ? 0x40 : 0)
							| (isPinHigh(dPins[1]) ? 0x20 : 0)
							| (isPinHigh(dPins[0]) ? 0x10 : 0);
					}
					tempDone = false;
				} else {
					if (isPinHigh(rwPin)) {
						if (isPinHigh(rsPin)) {
							var data = DDRAM[row][col];
							readData(data & 0x0F);
						} else {
							var data = col;
							readInstruction(data & 0x0F);
						}
					} else {
						tempByte |= (getByte() & 0x0F);
						if (isPinHigh(rsPin)) {
							writeData(tempByte);
						} else {
							writeInstruction(tempByte);
						}
					}
					tempDone = true;
				}
			}
			updateDDRAM();
		}
	};
	// default blinking settings
	var blinkingLoop = false;
	var blinking = function () {
		window.setInterval(function () {
			var tableRows = [];
			var tableRows0 = document.getElementsByClassName("table-row-0");
			for (var i = 0; i < tableRows0.length; i++) {
				tableRows.push(tableRows0.item(i));
			}
			var tableRows1 = document.getElementsByClassName("table-row-1");
			for (var i = 0; i < tableRows1.length; i++) {
				tableRows.push(tableRows1.item(i));
			}
			for (var r = 0; r < tableRows.length; r++) {
				var tableCols = tableRows[r].getElementsByClassName("table-col");
				for (var c = 0; c < tableCols.length; c++) {
					var offsetC = (c + offset) % MAX_COLS;
					tableCols.item(offsetC).getElementsByClassName("blink").item(0).setAttribute("style", (r == row && c == col && screenOn && blinkOn && blinkingLoop && (r == 0 || (r == 1 && rows))) ? "" : "display: none;");
				}
			}
			blinkingLoop = !blinkingLoop;
		}, 500);
	};
	blinking();
}

window.addEventListener("load", function (loadEvent) {
	var simulator = new HD44780Simulator();
	document.getElementById("pw").addEventListener("click", function (clickEvent) {
		simulator.pw();
	});
	document.getElementById("rs").addEventListener("click", function (clickEvent) {
		simulator.rs();
	});
	document.getElementById("rw").addEventListener("click", function (clickEvent) {
		simulator.rw();
	});
	document.getElementById("en").addEventListener("click", function (clickEvent) {
		simulator.en();
	});
	document.getElementById("d7").addEventListener("click", function (clickEvent) {
		simulator.ds(7);
	});
	document.getElementById("d6").addEventListener("click", function (clickEvent) {
		simulator.ds(6);
	});
	document.getElementById("d5").addEventListener("click", function (clickEvent) {
		simulator.ds(5);
	});
	document.getElementById("d4").addEventListener("click", function (clickEvent) {
		simulator.ds(4);
	});
	document.getElementById("d3").addEventListener("click", function (clickEvent) {
		simulator.ds(3);
	});
	document.getElementById("d2").addEventListener("click", function (clickEvent) {
		simulator.ds(2);
	});
	document.getElementById("d1").addEventListener("click", function (clickEvent) {
		simulator.ds(1);
	});
	document.getElementById("d0").addEventListener("click", function (clickEvent) {
		simulator.ds(0);
	});
});